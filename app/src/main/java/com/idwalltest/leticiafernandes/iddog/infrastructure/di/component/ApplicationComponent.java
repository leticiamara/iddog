package com.idwalltest.leticiafernandes.iddog.infrastructure.di.component;

import android.app.Application;

import com.idwalltest.leticiafernandes.iddog.infrastructure.data.SharedPreferencesManager;
import com.idwalltest.leticiafernandes.iddog.infrastructure.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by leticiafernandes on 10/04/18.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    Application providesApplication();

    SharedPreferencesManager provideSharedPreferencesManager();
}
