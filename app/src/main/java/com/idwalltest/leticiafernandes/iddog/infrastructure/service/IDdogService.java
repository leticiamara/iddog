package com.idwalltest.leticiafernandes.iddog.infrastructure.service;

import com.idwalltest.leticiafernandes.iddog.domain.model.Feed;
import com.idwalltest.leticiafernandes.iddog.domain.model.SignUpResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by leticiafernandes on 07/04/18.
 */

public interface IDdogService {

    @POST("/signup")
    Observable<SignUpResponse> signUp(@Header("email") String email);

    @GET("/feed")
    Observable<Feed> loadDefaultFeed(@Header("Authorization") String token);

    @GET("/feed")
    Observable<Feed> loadFeed(@Header("Authorization") String token, @Query("category") String category);
}
