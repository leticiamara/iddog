package com.idwalltest.leticiafernandes.iddog.infrastructure.di.module;

import com.idwalltest.leticiafernandes.iddog.domain.interactor.ISignUpInteractor;
import com.idwalltest.leticiafernandes.iddog.domain.interactor.SignUpInteractor;
import com.idwalltest.leticiafernandes.iddog.infrastructure.data.SharedPreferencesManager;
import com.idwalltest.leticiafernandes.iddog.presentation.presenter.ISignUpPresenter;
import com.idwalltest.leticiafernandes.iddog.presentation.presenter.SignUpPresenter;
import com.idwalltest.leticiafernandes.iddog.presentation.view.mvpView.ISignUpMvpView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by leticiafernandes on 10/04/18.
 */
@Module
public class SignUpModule {

    private ISignUpMvpView signUpMvpView;

    public SignUpModule(ISignUpMvpView signUpMvpView) {
        this.signUpMvpView = signUpMvpView;
    }

    @Provides
    public ISignUpPresenter provideSignUpPresenter(ISignUpInteractor signUpInteractor,
                                                   SharedPreferencesManager sharedPreferencesManager) {
        return new SignUpPresenter(signUpMvpView, signUpInteractor, sharedPreferencesManager);
    }

    @Provides
    public ISignUpInteractor provideSignUpInteractor() {
        return new SignUpInteractor();
    }
}
