package com.idwalltest.leticiafernandes.iddog.presentation.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.idwalltest.leticiafernandes.iddog.R;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leticiafernandes on 07/04/18.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.DogViewHolder> {

    private List<String> feedList;
    private OnItemClickListener onItemClickListener;

    public FeedAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public DogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dog, parent, false);
        return new DogViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(DogViewHolder holder, int position) {
        String imageURL = feedList.get(position);
        loadImage(holder, imageURL);
    }

    @Override
    public int getItemCount() {
        return feedList != null ? feedList.size() : 0;
    }

    public void setFeedList(List<String> feedList) {
        this.feedList = feedList;
        notifyDataSetChanged();
    }

    private void loadImage(DogViewHolder holder, String imageURL) {
        Picasso.with(holder.itemView.getContext())
                .load(imageURL)
                .centerCrop()
                .fit()
                .into(holder.getImgDog());
    }

    public String getFeedItem(int adapterPosition) {
        return feedList.get(adapterPosition);
    }

    public class DogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.img_dog)
        ImageView imgDog;
        private OnItemClickListener onItemClickListener;

        DogViewHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onItemClickListener = onItemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) onItemClickListener.onItemClicked(getAdapterPosition());
        }

        public ImageView getImgDog() {
            return imgDog;
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(int adapterPosition);
    }
}
