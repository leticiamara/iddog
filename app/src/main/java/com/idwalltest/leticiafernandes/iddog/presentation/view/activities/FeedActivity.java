package com.idwalltest.leticiafernandes.iddog.presentation.view.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.idwalltest.leticiafernandes.iddog.R;
import com.idwalltest.leticiafernandes.iddog.domain.model.FeedCategory;
import com.idwalltest.leticiafernandes.iddog.presentation.view.BottomNavigationViewHelper;
import com.idwalltest.leticiafernandes.iddog.presentation.view.fragments.FeedFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedActivity extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView navigationView;
    @BindView(R.id.feed_frame)
    FrameLayout feedFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        ButterKnife.bind(this);

        navigationView.setOnNavigationItemSelectedListener(this);
        BottomNavigationViewHelper.removeShiftMode(navigationView);

        FeedFragment huskyFragment = FeedFragment.newInstance(FeedCategory.HUSKY);
        getSupportFragmentManager().beginTransaction().replace(R.id.feed_frame, huskyFragment).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_husky:
                FeedFragment huskyFragment = FeedFragment.newInstance(FeedCategory.HUSKY);
                getSupportFragmentManager().beginTransaction().replace(R.id.feed_frame, huskyFragment).commit();
                return true;
            case R.id.navigation_hound:
                FeedFragment houndFragment = FeedFragment.newInstance(FeedCategory.HOUND);
                getSupportFragmentManager().beginTransaction().replace(R.id.feed_frame, houndFragment).commit();
                return true;
            case R.id.navigation_pug:
                FeedFragment pugFragment = FeedFragment.newInstance(FeedCategory.PUG);
                getSupportFragmentManager().beginTransaction().replace(R.id.feed_frame, pugFragment).commit();
                return true;
            case R.id.navigation_labrador:
                FeedFragment labradorFragment = FeedFragment.newInstance(FeedCategory.LABRADOR);
                getSupportFragmentManager().beginTransaction().replace(R.id.feed_frame, labradorFragment).commit();
                return true;
        }
        return false;
    }
}
