package com.idwalltest.leticiafernandes.iddog.presentation.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.idwalltest.leticiafernandes.iddog.R;
import com.idwalltest.leticiafernandes.iddog.domain.model.FeedCategory;
import com.idwalltest.leticiafernandes.iddog.infrastructure.Application;
import com.idwalltest.leticiafernandes.iddog.infrastructure.di.component.DaggerFeedComponent;
import com.idwalltest.leticiafernandes.iddog.infrastructure.di.module.FeedModule;
import com.idwalltest.leticiafernandes.iddog.presentation.presenter.IFeedPresenter;
import com.idwalltest.leticiafernandes.iddog.presentation.view.adapters.FeedAdapter;
import com.idwalltest.leticiafernandes.iddog.presentation.view.mvpView.IFeedMvpView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leticiafernandes on 13/04/18.
 */

public class FeedFragment extends Fragment implements FeedAdapter.OnItemClickListener, IFeedMvpView {

    private static FeedCategory feedCategory;
    @BindView(R.id.rv_feed)
    RecyclerView rvHusky;
    private FeedAdapter feedAdapter;
    @Inject
    IFeedPresenter feedPresenter;

    public static FeedFragment newInstance(FeedCategory feedCategory) {
        FeedFragment feedFragment = new FeedFragment();
        FeedFragment.feedCategory = feedCategory;
        return feedFragment;
    }

    public FeedFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.husky_fragment, container, false);
        ButterKnife.bind(this, rootView);

        setUpRecyclerView();
        setUpDagger();
        loadData(feedCategory);
        return rootView;
    }

    @Override
    public void onItemClicked(int adapterPosition) {
        FeedImageDetailDialogFragment dialogFragment = FeedImageDetailDialogFragment
                .newInstance(feedAdapter.getFeedItem(adapterPosition));
        dialogFragment.show(getFragmentManager(), "FeedImageDetailDialogFragment");
    }

    @Override
    public void showFeedList(List<String> feedList) {
        feedAdapter.setFeedList(feedList);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void loadData(FeedCategory feedCategory) {
        switch (feedCategory) {
            case HUSKY:
                feedPresenter.loadDefaultFeedData();
                break;
            case HOUND:
                feedPresenter.loadHoundData();
                break;
            case PUG:
                feedPresenter.loadPugData();
                break;
            case LABRADOR:
                feedPresenter.loadLabradorData();
                break;
        }
    }

    private void setUpRecyclerView() {
        int numberOfColumns = 3;
        rvHusky.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        feedAdapter = new FeedAdapter(this);
        rvHusky.setAdapter(feedAdapter);
    }

    private void setUpDagger() {
        DaggerFeedComponent.builder()
                .applicationComponent(Application.getsInstance().getApplicationComponent())
                .feedModule(new FeedModule(this))
                .build()
                .inject(this);
    }
}
