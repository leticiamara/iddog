package com.idwalltest.leticiafernandes.iddog.infrastructure;

import com.idwalltest.leticiafernandes.iddog.infrastructure.di.component.ApplicationComponent;
import com.idwalltest.leticiafernandes.iddog.infrastructure.di.component.DaggerApplicationComponent;
import com.idwalltest.leticiafernandes.iddog.infrastructure.di.module.ApplicationModule;

/**
 * Created by leticiafernandes on 10/04/18.
 */

public class Application extends android.app.Application {

    private static Application sInstance;
    private ApplicationComponent applicationComponent;

    public static Application getsInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        initializeInjector();
    }

    private void initializeInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
