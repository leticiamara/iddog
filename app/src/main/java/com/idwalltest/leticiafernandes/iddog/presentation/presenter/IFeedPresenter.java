package com.idwalltest.leticiafernandes.iddog.presentation.presenter;

/**
 * Created by leticiafernandes on 10/04/18.
 */

public interface IFeedPresenter {
    void loadDefaultFeedData();

    void loadHoundData();

    void loadPugData();

    void loadLabradorData();
}
