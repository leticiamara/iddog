package com.idwalltest.leticiafernandes.iddog.infrastructure.service;

/**
 * Created by leticiafernandes on 11/04/18.
 */

public class ErrorMessage {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
