package com.idwalltest.leticiafernandes.iddog.domain.interactor;

import com.idwalltest.leticiafernandes.iddog.domain.model.SignUpResponse;

import io.reactivex.Observable;

/**
 * Created by leticiafernandes on 10/04/18.
 */

public interface ISignUpInteractor {
    Observable<SignUpResponse> signUp(String email);
}
