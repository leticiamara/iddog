package com.idwalltest.leticiafernandes.iddog.infrastructure.di.component;

import com.idwalltest.leticiafernandes.iddog.infrastructure.di.PerActivity;
import com.idwalltest.leticiafernandes.iddog.infrastructure.di.module.FeedModule;
import com.idwalltest.leticiafernandes.iddog.presentation.presenter.IFeedPresenter;
import com.idwalltest.leticiafernandes.iddog.presentation.view.fragments.FeedFragment;

import dagger.Component;

/**
 * Created by leticiafernandes on 10/04/18.
 */
@PerActivity
@Component(modules = {FeedModule.class}, dependencies = ApplicationComponent.class)
public interface FeedComponent {

    IFeedPresenter getFeedPresenter();

    void inject(FeedFragment feedFragment);
}
