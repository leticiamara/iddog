package com.idwalltest.leticiafernandes.iddog.infrastructure.di.component;

import com.idwalltest.leticiafernandes.iddog.infrastructure.di.PerActivity;
import com.idwalltest.leticiafernandes.iddog.infrastructure.di.module.SignUpModule;
import com.idwalltest.leticiafernandes.iddog.presentation.presenter.ISignUpPresenter;
import com.idwalltest.leticiafernandes.iddog.presentation.view.activities.SignUpActivity;

import dagger.Component;

/**
 * Created by leticiafernandes on 10/04/18.
 */
@PerActivity
@Component(modules = {SignUpModule.class}, dependencies = ApplicationComponent.class)
public interface SignUpComponent {

    ISignUpPresenter getSignUpPresenter();

    void inject(SignUpActivity signUpActivity);
}
