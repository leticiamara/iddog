package com.idwalltest.leticiafernandes.iddog.domain.model;

/**
 * Created by leticiafernandes on 11/04/18.
 */

public enum FeedCategory {
    HUSKY, HOUND, PUG, LABRADOR
}
