package com.idwalltest.leticiafernandes.iddog.presentation.presenter;

/**
 * Created by leticiafernandes on 10/04/18.
 */

public interface ISignUpPresenter {
    void signUp(String email);

    void checkIfTokenIsSaved();
}
