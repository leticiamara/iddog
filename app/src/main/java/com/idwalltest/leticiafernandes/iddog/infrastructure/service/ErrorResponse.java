package com.idwalltest.leticiafernandes.iddog.infrastructure.service;

import com.google.gson.annotations.SerializedName;

/**
 * Created by leticiafernandes on 11/04/18.
 */

public class ErrorResponse {

    @SerializedName("error")
    private ErrorMessage errorMessage;

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }
}
