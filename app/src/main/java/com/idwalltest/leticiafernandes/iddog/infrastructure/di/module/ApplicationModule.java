package com.idwalltest.leticiafernandes.iddog.infrastructure.di.module;

import android.app.Application;

import com.idwalltest.leticiafernandes.iddog.infrastructure.data.SharedPreferencesManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by leticiafernandes on 10/04/18.
 */
@Module
public class ApplicationModule {

    private Application mApplication;
    public ApplicationModule(Application application) {
        mApplication = application;
    }
    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }
    @Provides
    public SharedPreferencesManager provideSharedPreferencesManager() {
        return new SharedPreferencesManager(mApplication.getApplicationContext());
    }
}
