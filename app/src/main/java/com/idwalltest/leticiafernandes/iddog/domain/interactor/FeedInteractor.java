package com.idwalltest.leticiafernandes.iddog.domain.interactor;

import com.idwalltest.leticiafernandes.iddog.domain.model.Feed;
import com.idwalltest.leticiafernandes.iddog.infrastructure.service.IDdogService;
import com.idwalltest.leticiafernandes.iddog.infrastructure.service.RetrofitHelper;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by leticiafernandes on 08/04/18.
 */

public class FeedInteractor implements IFeedInteractor {

    @Override
    public Observable<Feed> loadDefaultFeed(String token) {
        IDdogService iDdogService = RetrofitHelper.getRetrofit().create(IDdogService.class);
        return iDdogService.loadDefaultFeed(token)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Feed> loadFeed(String token, String category) {
        IDdogService iDdogService = RetrofitHelper.getRetrofit().create(IDdogService.class);
        return iDdogService.loadFeed(token, category)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
