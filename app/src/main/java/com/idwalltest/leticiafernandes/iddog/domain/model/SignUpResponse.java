package com.idwalltest.leticiafernandes.iddog.domain.model;

/**
 * Created by leticiafernandes on 07/04/18.
 */

public class SignUpResponse {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
