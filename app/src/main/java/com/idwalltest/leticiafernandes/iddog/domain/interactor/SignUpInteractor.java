package com.idwalltest.leticiafernandes.iddog.domain.interactor;

import com.idwalltest.leticiafernandes.iddog.domain.model.SignUpResponse;
import com.idwalltest.leticiafernandes.iddog.infrastructure.service.IDdogService;
import com.idwalltest.leticiafernandes.iddog.infrastructure.service.RetrofitHelper;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by leticiafernandes on 07/04/18.
 */

public class SignUpInteractor implements ISignUpInteractor {

    @Override
    public Observable<SignUpResponse> signUp(String email) {
        IDdogService iDdogService = RetrofitHelper.getRetrofit().create(IDdogService.class);
        return iDdogService.signUp(email)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
