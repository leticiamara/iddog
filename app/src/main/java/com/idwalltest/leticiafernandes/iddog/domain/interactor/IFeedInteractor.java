package com.idwalltest.leticiafernandes.iddog.domain.interactor;

import com.idwalltest.leticiafernandes.iddog.domain.model.Feed;

import io.reactivex.Observable;

/**
 * Created by leticiafernandes on 10/04/18.
 */

public interface IFeedInteractor {
    Observable<Feed> loadDefaultFeed(String token);

    Observable<Feed> loadFeed(String userToken, String category);
}
