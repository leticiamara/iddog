package com.idwalltest.leticiafernandes.iddog.presentation.view.mvpView;

/**
 * Created by leticiafernandes on 07/04/18.
 */

public interface ISignUpMvpView {
    void goToFeedScreen();

    void showMessage(String message);
}
