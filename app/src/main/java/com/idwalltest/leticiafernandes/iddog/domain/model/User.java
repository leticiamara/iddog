package com.idwalltest.leticiafernandes.iddog.domain.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by leticiafernandes on 07/04/18.
 */
public class User {

    @SerializedName("_id")
    private String id;
    private String token;
    private Date createdAt;
    private Date updatedAt;
    @SerializedName("__v")
    private int version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
