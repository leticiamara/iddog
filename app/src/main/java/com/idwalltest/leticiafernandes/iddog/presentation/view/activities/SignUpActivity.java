package com.idwalltest.leticiafernandes.iddog.presentation.view.activities;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.idwalltest.leticiafernandes.iddog.R;
import com.idwalltest.leticiafernandes.iddog.domain.interactor.SignUpInteractor;
import com.idwalltest.leticiafernandes.iddog.infrastructure.Application;
import com.idwalltest.leticiafernandes.iddog.infrastructure.data.SharedPreferencesManager;
import com.idwalltest.leticiafernandes.iddog.infrastructure.di.component.DaggerSignUpComponent;
import com.idwalltest.leticiafernandes.iddog.infrastructure.di.module.SignUpModule;
import com.idwalltest.leticiafernandes.iddog.presentation.presenter.ISignUpPresenter;
import com.idwalltest.leticiafernandes.iddog.presentation.presenter.SignUpPresenter;
import com.idwalltest.leticiafernandes.iddog.presentation.view.mvpView.ISignUpMvpView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity implements ISignUpMvpView {

    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.til_email)
    TextInputLayout textInputEmail;
    @Inject
    ISignUpPresenter signUpPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        setUpDagger();
        signUpPresenter.checkIfTokenIsSaved();
    }

    @OnClick(R.id.btn_signup)
    public void signUp() {
        if (!editEmail.getText().toString().isEmpty()) {
            signUpPresenter.signUp(editEmail.getText().toString());
            textInputEmail.setError(null);
        } else {
            textInputEmail.setError(getString(R.string.erro_email_required));
        }
    }

    @Override
    public void goToFeedScreen() {
        Intent intent = new Intent(this, FeedActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void setUpDagger() {
        DaggerSignUpComponent.builder()
                .applicationComponent(Application.getsInstance().getApplicationComponent())
                .signUpModule(new SignUpModule(this))
                .build()
                .inject(this);
    }
}
