package com.idwalltest.leticiafernandes.iddog.infrastructure.di;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by leticiafernandes on 10/04/18.
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {
}
