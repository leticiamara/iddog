package com.idwalltest.leticiafernandes.iddog.presentation.presenter;

import com.idwalltest.leticiafernandes.iddog.domain.interactor.ISignUpInteractor;
import com.idwalltest.leticiafernandes.iddog.domain.interactor.SignUpInteractor;
import com.idwalltest.leticiafernandes.iddog.infrastructure.data.SharedPreferencesManager;
import com.idwalltest.leticiafernandes.iddog.infrastructure.service.RetrofitHelper;
import com.idwalltest.leticiafernandes.iddog.presentation.view.mvpView.ISignUpMvpView;

/**
 * Created by leticiafernandes on 07/04/18.
 */

public class SignUpPresenter implements ISignUpPresenter {

    private ISignUpMvpView mvpView;
    private SharedPreferencesManager sharedPreferencesManager;
    private ISignUpInteractor signUpInteractor;

    public SignUpPresenter(ISignUpMvpView mvpView,
                           ISignUpInteractor signUpInteractor,
                           SharedPreferencesManager sharedPreferencesManager) {
        this.mvpView = mvpView;
        this.signUpInteractor = signUpInteractor;
        this.sharedPreferencesManager = sharedPreferencesManager;
    }

    @Override
    public void signUp(String email) {
        signUpInteractor.signUp(email)
                .subscribe(signUpResponse -> {
                    sharedPreferencesManager.saveUserToken(signUpResponse.getUser().getToken());
                    mvpView.goToFeedScreen();
                }, throwable -> mvpView.showMessage(RetrofitHelper.convertThrowableToErrorMessage(
                        throwable)));
    }

    @Override
    public void checkIfTokenIsSaved() {
        if (!sharedPreferencesManager.getUserToken().isEmpty()) {
            mvpView.goToFeedScreen();
        }
    }
}
