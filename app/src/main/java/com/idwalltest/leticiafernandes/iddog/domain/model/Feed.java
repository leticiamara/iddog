package com.idwalltest.leticiafernandes.iddog.domain.model;

import java.util.List;

/**
 * Created by leticiafernandes on 08/04/18.
 */

public class Feed {

    private String category;
    private List<String> list;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
}
