package com.idwalltest.leticiafernandes.iddog.infrastructure.service;

import com.idwalltest.leticiafernandes.iddog.BuildConfig;
import com.idwalltest.leticiafernandes.iddog.infrastructure.Application;
import com.idwalltest.leticiafernandes.iddog.infrastructure.utils.NetworkUtils;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by leticiafernandes on 07/04/18.
 */

public class RetrofitHelper {

    private static Converter<ResponseBody, ErrorResponse> errorConverter;

    public static Retrofit getRetrofit() {
        int cacheSize = 10 * 1024 * 1024; // 10 MB
        Cache cache = new Cache(Application.getsInstance().getApplicationContext().getCacheDir(),
                cacheSize);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(getCacheInterceptor())
                .cache(cache)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.END_POINT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        errorConverter = retrofit
                .responseBodyConverter(ErrorResponse.class, new Annotation[0]);

        return retrofit;
    }

    private static Interceptor getCacheInterceptor() {
        return chain -> {
            Response originalResponse = chain.proceed(chain.request());
            if (NetworkUtils.isNetworkAvailable(Application.getsInstance().getApplicationContext())) {
                int maxAge = 60 * 60; // read from cache for 1 hour
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, max-age=" + maxAge)
                        .build();
            } else {
                int maxStale = 60 * 60 * 24 * 2; // tolerate 2-days stale
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                        .build();
            }
        };
    }

    public static String convertThrowableToErrorMessage(Throwable throwable) throws IOException {
        if (throwable instanceof HttpException) {
            ResponseBody responseBody = ((HttpException) throwable).response().errorBody();
            if (errorConverter != null && responseBody != null) {
                ErrorResponse error = errorConverter.convert(responseBody);
                return error.getErrorMessage().getMessage();
            }
        }
        return throwable.getMessage();
    }
}
