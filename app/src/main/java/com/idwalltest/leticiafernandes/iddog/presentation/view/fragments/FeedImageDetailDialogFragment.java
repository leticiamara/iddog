package com.idwalltest.leticiafernandes.iddog.presentation.view.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.idwalltest.leticiafernandes.iddog.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leticiafernandes on 13/04/18.
 */

public class FeedImageDetailDialogFragment extends DialogFragment {

    @BindView(R.id.img_feed_item)
    ImageView imgFeedItemDetail;
    @BindView(R.id.feed_img_container)
    ConstraintLayout feedImgContainer;

    private static final String IMAGE_URL_KEY = "image-url-key";

    public static FeedImageDetailDialogFragment newInstance(String imageURL) {
        FeedImageDetailDialogFragment fragment = new FeedImageDetailDialogFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_URL_KEY, imageURL);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String imageURL = getArguments().getString(IMAGE_URL_KEY, "");

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_feed_image_detail, null);
        ButterKnife.bind(this, dialogView);

        Picasso.with(getActivity())
                .load(imageURL)
                .fit()
                .centerInside()
                .into(imgFeedItemDetail);

        feedImgContainer.setOnClickListener(view -> {
                    getDialog().dismiss();
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);

        AlertDialog dialog = builder.create();

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        return dialog;
    }
}
