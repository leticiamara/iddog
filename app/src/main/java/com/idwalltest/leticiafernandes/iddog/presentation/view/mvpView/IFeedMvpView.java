package com.idwalltest.leticiafernandes.iddog.presentation.view.mvpView;

import com.idwalltest.leticiafernandes.iddog.domain.model.Feed;

import java.util.List;

/**
 * Created by leticiafernandes on 08/04/18.
 */

public interface IFeedMvpView {
    void showFeedList(List<String> feedList);

    void showMessage(String message);
}
