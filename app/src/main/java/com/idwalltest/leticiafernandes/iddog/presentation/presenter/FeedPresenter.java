package com.idwalltest.leticiafernandes.iddog.presentation.presenter;

import com.idwalltest.leticiafernandes.iddog.domain.interactor.FeedInteractor;
import com.idwalltest.leticiafernandes.iddog.domain.interactor.IFeedInteractor;
import com.idwalltest.leticiafernandes.iddog.domain.model.Feed;
import com.idwalltest.leticiafernandes.iddog.domain.model.FeedCategory;
import com.idwalltest.leticiafernandes.iddog.infrastructure.data.SharedPreferencesManager;
import com.idwalltest.leticiafernandes.iddog.infrastructure.service.RetrofitHelper;
import com.idwalltest.leticiafernandes.iddog.presentation.view.mvpView.IFeedMvpView;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

/**
 * Created by leticiafernandes on 07/04/18.
 */

public class FeedPresenter implements IFeedPresenter {

    private IFeedMvpView mvpView;
    private IFeedInteractor feedInteractor;
    private SharedPreferencesManager sharedPreferencesManager;

    public FeedPresenter(IFeedMvpView mvpView, IFeedInteractor feedInteractor,
                         SharedPreferencesManager sharedPreferencesManager) {
        this.mvpView = mvpView;
        this.feedInteractor = feedInteractor;
        this.sharedPreferencesManager = sharedPreferencesManager;
    }

    @Override
    public void loadDefaultFeedData() {
        feedInteractor.loadDefaultFeed(sharedPreferencesManager.getUserToken())
                .subscribe(feed -> {
                    mvpView.showFeedList(feed.getList());},
                        throwable -> {
                    mvpView.showMessage(RetrofitHelper.convertThrowableToErrorMessage(throwable));
                });
    }

    @Override
    public void loadHoundData() {
        feedInteractor.loadFeed(sharedPreferencesManager.getUserToken(),
                FeedCategory.HOUND.toString().toLowerCase())
                .subscribe(feed -> {mvpView.showFeedList(feed.getList());},
                        throwable -> mvpView.showMessage(RetrofitHelper.convertThrowableToErrorMessage(throwable)));
    }

    @Override
    public void loadPugData() {
        feedInteractor.loadFeed(sharedPreferencesManager.getUserToken(),
                FeedCategory.PUG.toString().toLowerCase())
                .subscribe(feed -> {mvpView.showFeedList(feed.getList());},
                        throwable -> mvpView.showMessage(RetrofitHelper.convertThrowableToErrorMessage(throwable)));
    }

    @Override
    public void loadLabradorData() {
        feedInteractor.loadFeed(sharedPreferencesManager.getUserToken(),
                FeedCategory.LABRADOR.toString().toLowerCase())
                .subscribe(feed -> {mvpView.showFeedList(feed.getList());},
                        throwable -> mvpView.showMessage(RetrofitHelper.convertThrowableToErrorMessage(throwable)));
    }
}
