package com.idwalltest.leticiafernandes.iddog.infrastructure.di.module;

import com.idwalltest.leticiafernandes.iddog.domain.interactor.FeedInteractor;
import com.idwalltest.leticiafernandes.iddog.domain.interactor.IFeedInteractor;
import com.idwalltest.leticiafernandes.iddog.infrastructure.data.SharedPreferencesManager;
import com.idwalltest.leticiafernandes.iddog.presentation.presenter.FeedPresenter;
import com.idwalltest.leticiafernandes.iddog.presentation.presenter.IFeedPresenter;
import com.idwalltest.leticiafernandes.iddog.presentation.view.mvpView.IFeedMvpView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by leticiafernandes on 10/04/18.
 */
@Module
public class FeedModule {

    private IFeedMvpView mvpView;

    public FeedModule(IFeedMvpView mvpView) {
        this.mvpView = mvpView;
    }

    @Provides
    public IFeedPresenter provideFeedPresenter(IFeedInteractor feedInteractor,
                                               SharedPreferencesManager sharedPreferencesManager) {
        return new FeedPresenter(mvpView, feedInteractor, sharedPreferencesManager);
    }

    @Provides
    public IFeedInteractor provideFeedInteractor() {
        return new FeedInteractor();
    }
}
